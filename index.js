const form = document.getElementById("addForm");
let mod = document.getElementById("addButton");
const close = document.getElementById("closeForm");

mod.onclick = function () {
  console.log(6);
  form.style.left = "0%";
};

close.onclick = function () {
  document.getElementById("firstName").value = "";
  document.getElementById("secondName").value = "";
  document.getElementById("email").value = "";
  form.style.left = "-100%";
};

function sub() {
  let firstName = document.getElementById("firstName").value;
  let secondName = document.getElementById("secondName").value;
  let email = document.getElementById("email").value;
  
  let firstExp = new RegExp('[A-Za-z]{1,50}');
  let secondExp = new RegExp('[A-Za-z]{1,50}');
  let mailExp = new RegExp('[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+(?:[A-Z]{2}|com|org|net|gov|mil|biz|info|mobi|name|aero|jobs|museum)\\b');
  
  if (!firstExp.test(firstName) || !secondExp.test(secondName)) {
    document.getElementById('warn').innerHTML = "";
    document.getElementById('warn').innerHTML = "Invalid input some of names!";
    return false;
  } else if (!mailExp.test(email)) {
    document.getElementById('warn').innerHTML = "";
    document.getElementById('warn').innerHTML = "Invalid input of E-mail!";
    return false;
  } else {
    return true;
  }
}

function getSuccessOutput() {
  if(!sub()) {
    return false;
  } else {
    getRequest(
      './php/push.php',
      drawOutput,
      drawError
    );
  }
}

function drawError () {
  var container = document.getElementById('output');
  container.innerHTML = 'Bummer: there was an error!';
}

function drawOutput(responseText) {
  var container = document.getElementById('output');
  container.innerHTML = responseText;
}

function getRequest(url, success, error) {
  var req = false;
  try{
    req = new XMLHttpRequest();
  } catch (e){
    try{
      req = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
      try{
        req = new ActiveXObject("Microsoft.XMLHTTP");
      } catch (e){
        return false;
      }
    }
  }
  if (!req) return false;
  if (typeof success != 'function') success = function () {};
  if (typeof error!= 'function') error = function () {};
  req.onreadystatechange = function(){
    if(req .readyState === 4){
      return req.status === 200 ?
        success(req.responseText) : error(req.status)
        ;
    }
  };
  req.open("GET", url, true);
  req.send(null);
  return req;
}