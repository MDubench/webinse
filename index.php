<?php
    require_once 'php/database.php';
    require_once 'php/function.php';
    require_once 'php/push.php';
    // require_once 'php/rewrite.php';
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="style/index.css">
    <script src="index.js"></script>
    <title>List of people</title>
</head>
<body>
    <div id="addButton"></div>
<div class="table">
        <div class="table_row table_row--header">
            <div class="table_header">First name</div>
            <div class="table_header">Second name</div>
            <div class="table_header">E-mail</div>
        </div>
        <?php
            $peop = get_people($link);
        ?>
        <?php if (count($peop) === 0):?>
        <div class="table_row table_row--person">
            <div>No connection with database</div>
        </div>
        <?php else: ?>

        <?php foreach ($peop as $man):?>
        <div class="table_row table_row--person" id=<?=$man["id"]?>>
            <div class="table_first"><?=$man["first_name"]?></div>
            <div class="table_second"><?=$man["second_name"]?></div>
            <div class="table_email"> <a href="mailto:<?=$man["email"]?>"><?=$man["email"]?></a>
               <div class="table_controllers">
                    <a href='?edit_id=<?=$man["id"]?>'>
                    <img id=<?=$man["id"]?> class = "table_edit" src="style/edit.png"/></a>
                    <a href='?del_id=<?=$man["id"]?>'>
                    <img id=<?=$man["id"]?> class = "table_delete" src="style/delete.png"/></a>
               </div>
            </div>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
</div>

<?php
            $persone = get_persone($link);
        ?>
        <?php if (count($persone) === 0):?>
        <div class="table_row table_row--person">
            <div>No connection with database</div>
        </div>
        <?php else: ?>


<div class="form" id="editForm">
    <button class="form_close" id="closeForm">Close</button>
    <form id="form" onsubmit="return getRewriteData()">
        <div class="form_warn" id="warn"></div>
        <input class="form_text form_text--firstName" id="firstNameRew" type="text" placeholder="<?=$persone["first_name"]?>" name="first_name" required />
        <input class="form_text form_text--secondName" id="<?=$persone["second_name"]?>" type="text" placeholder="persone" name="second_name"required />
        <input class="form_text form_text--email" id="emailRew" type="email" placeholder="<?=$persone["mail"]?>" name="email" required />
        <div class="form_buttons">
            <input class="form_reset"  type="reset" id="resetForm"/>
            <input class="form_submit"  type="submit" id="submitForm"/>
        </div>
    </form>
    <?php endif; ?>
</div>


<div class="form" id="addForm">
    <button class="form_close" id="closeForm">Close</button>
    <form id="form" onsubmit="return getSuccessOutput()">
        <div class="form_warn" id="warn"></div>
        <input class="form_text form_text--firstName" id="firstName" type="text" placeholder="First name" name="first_name" required />
        <input class="form_text form_text--secondName" id="secondName" type="text" placeholder="Second name" name="second_name"required />
        <input class="form_text form_text--email" id="email" type="email" placeholder="E-mail" name="email" required />
        <div class="form_buttons">
            <input class="form_reset"  type="reset" id="resetForm"/>
            <input class="form_submit"  type="submit" id="submitForm"/>
        </div>
    </form>
</div>
</body>
</html>